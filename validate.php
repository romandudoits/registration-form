<?php
//валидирование данных на xss
function validate($value)
{
    $value = trim($value);
    $value = stripslashes($value);
    $value = strip_tags($value);
    $value = htmlspecialchars($value);
    return $value;
}

$login = validate($_POST['login']);
$pass = validate($_POST['password1']);
$passcon = validate($_POST['password2']);
$email = validate($_POST['email']);
$name = validate($_POST['name']);
