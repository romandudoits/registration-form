<?php
require 'db.php';
require 'validate.php';

if (isset($_POST["login"]) && isset($_POST["password1"]) && isset($_POST["password2"]) && isset($_POST["email"])
    && isset($_POST["name"])) {
// Проверяем валидность Login
    if (mb_strlen($login) < 6) {
        $_SESSION['errorLogin'] = " должно быть не мение 6 символов";
    } elseif (!preg_match("/^[A-Za-zА-Яа-яЁё0-9]+$/", $login)) {
        $_SESSION['errorLogin'] = "Только буквы и цифры";
    } else unset($_SESSION['errorLogin']);

    if (!isset($_SESSION['errorLogin'])) {
        foreach ($xml as $checkStatus) {
            if ($checkStatus->login == $login) {
                $_SESSION['errorLogin'] = 'Пользователь с таким Login существует';
                break;
            }
        }
    }
    // Проверяем валидность пароля
    if (!preg_match("/(?=^.{6,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/", $pass)) {
        $_SESSION['errorPass'] = "Пароль должен содержать цифру, буквы в разных регистрах и спец символ (знаки)";
    } else unset($_SESSION['errorPass']);
    if ($pass !== $passcon) {
        $_SESSION['errorPassCon'] = "Пароли не совпадают";
    } else unset($_SESSION['errorPassCon']);
    // Проверяем валидность Email
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $_SESSION['errorEmail'] = "Неправильный формат почты";
    } else unset($_SESSION['errorEmail']);

    if (!isset($_SESSION['errorEmail'])) {
        foreach ($xml as $checkStatus) {
            if ($checkStatus->email == $email) {
                $_SESSION['errorEmail'] = 'Пользователь с таким Email существует';
                break;
            }
        }
    }
    // Проверяем валидность name
    if (mb_strlen($name) != 2) {
        $_SESSION['errorName'] = "Длина имени должна быть в 2 символа";
    } else if (!preg_match("/^[A-Za-zА-Яа-яЁё0-9]+$/", $name)) {
        $_SESSION['errorName'] = "Только буквы и цифры";
    } else unset($_SESSION['errorName']);

//    если проходит валидацию регистрируем пользователя
    if (!isset($_SESSION['errorName']) && !isset($_SESSION['errorEmail']) && !isset($_SESSION['errorPassCon']) &&
        !isset($_SESSION['errorPass']) && !isset($_SESSION['errorLogin'])) {
        // шифруем пароль
        $pass = md5($pass . "qaKz123vfrQXtd6sSq4w");
        createElement($login, $pass, $email, $name, $xml);
        setcookie('user', $login, time() + 3600, "/");
    }


// формируем массив для json
    $result = array(
        'login' => $_SESSION['errorLogin'],
        'pass1' => $_SESSION['errorPass'],
        'pass2' => $_SESSION['errorPassCon'],
        'email' => $_SESSION['errorEmail'],
        'name' => $_SESSION['errorName'],
    );
// Переводим массив в JSON

    echo json_encode($result);

}







