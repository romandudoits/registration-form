<?php
//запускаем сессию и подключаемся к бд
session_start();
$xml = simplexml_load_file('users.xml');
$xml = new SimpleXMLElement('users.xml', null, true);


// создаём нового юзера
// в функцию передаём 5 параметров
// $xml это обЪект позволяющий обрабатывать xml документ
function createElement($login, $pass, $email, $name, $xml)
{
    $user = $xml->addChild('user');
    $user->addChild("login", $login);
    $user->addChild("pass", $pass);
    $user->addChild("email", $email);
    $user->addChild("name", $name);
    saveDocXML($xml);
}
// Ищем юзера по одному из 3 параметров
// name, login, email их передаём как $element
//$ value значение по которому ищем
function readElement($element, $value, $xml) {
    foreach ($xml as $readElem) {
        if ($readElem->$element == $value) {
            echo "Login : " . $readElem->login . "<br>";
            echo "Name : " . $readElem->name  . "<br>";
            echo "Email : " . $readElem->email . "<br>";
            break;
        }
    }
    if ($readElem->$element != $value){
        echo "Пользователь с таким $element не существует!!";
    }
}

//Обновление элемента
//$upValue значение на которое надо заменить
function updeteElement($element, $value, $xml, $upValue) {
    foreach ($xml as $readElem) {
        if ($readElem->$element == $value) {
            $readElem->$element = $upValue;
            saveDocXML($xml);
            echo "Замена $element произведена успешно";
            break;
        }
    }
    if ($readElem->$element != $upValue){
        echo "Пользователь с таким $element не существует!!";
    }
}

// Удаление элемента
function deleteElement($element, $value, $xml) {
    foreach ($xml as $readElem) {
        if ($readElem->$element == $value) {
            $delete=dom_import_simplexml($readElem);
            $delete->parentNode->removeChild($delete);
            saveDocXML($xml);
            echo "Удаление пользователя с $element $value произведена успешно";
            break;
        }
    }
    if ($readElem->$element != $value){
        echo "Пользователь с таким $element не существует!!";
    }
}

// метод для сохранения изменений в xml файл
function saveDocXML ($xml) {
    $dom = new DOMDocument('1.0');
    $dom->preserveWhiteSpace = false;
    $dom->formatOutput = true;
    $dom->loadXML($xml->asXML());
    $dom->save('users.xml');
}