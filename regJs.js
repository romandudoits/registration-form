// после считывания страницы запускается js код
$( document ).ready(function() {
    $("#btn").click(
        // при нажатие кнопки происходит отправка данных
        function() {
            sendAjaxForm('error_form', 'form', 'regValid.php');
            return false;
        }
    );
});

function sendAjaxForm(error_form, form, url) {
    $.ajax({
        url:     url, //url страницы
        type:     "POST", //метод отправки
        dataType: "html", //формат данных
        data: $("#"+form).serialize(),  // Сеарилизуем объект
        success: function(response) {//Данные отправлены успешно
            result = $.parseJSON(response);
            if (typeof result.login === "string" || typeof result.pass1 === "string" || typeof result.pass2 === "string"
                || typeof result.email === "string" || typeof result.name === "string") {
                $('#login').html(result.login);
                $('#password1').html(result.pass1);
                $('#password2').html(result.pass2);
                $('#email').html(result.email);
                $('#name').html(result.name);
                // если нет ошибок переходим на страницу приветствия
            } else window.location.href = 'pageHello.php';
        },
        error: function(response) { // Данные не отправлены
            $('#error_form').html('Ошибка. Данные не отправлены.');
        }
    });
}