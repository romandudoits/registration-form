
$( document ).ready(function() {
    $("#btn").click(
        // при нажатие копки происхожит отправка данных
        function() {
            sendAjaxForm('error_form', 'form', 'auth.php');
            return false;
        }
    );
});

function sendAjaxForm(error_form, form, url) {
    $.ajax({
        url:  url, //url страницы
        type: "POST", //метод отправки
        dataType: "html", //формат данных
        data: $("#"+form).serialize(),  // Сеарилизуем объект
        success: function(response) { //Данные отправлены успешно
            result = $.parseJSON(response);
            if (typeof result.login === "string" || typeof result.pass === "string" ) {
                $('#login').html(result.login);
                $('#password1').html(result.pass);
                // если нет ошибок переходим на страницу приветствия
            } else window.location.href = 'pageHello.php';
        },
        error: function(response) { // Данные не отправлены
            $('#error_form').html('Ошибка. Данные не отправлены.');
        }
    });
}
